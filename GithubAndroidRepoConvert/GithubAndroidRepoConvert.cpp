// GithubAndroidRepoConvert.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

#include <vector>

#include <omp.h>
#include <stdlib.h>
ifstream input;

#define MAX_BUFF_SIZE	4096

#define MIRROR "https://63.233.189.166/"

enum MODE{
	download,
	clean,
	update,
};

MODE mode = download;

struct BLOCK{
	string a;
	string b;
	string c;
};

vector<BLOCK> poll;

bool str_cmp(const char *str1,int off)
{
	char cmp[] = "name=";
	if (strlen(str1) - off < strlen(cmp))
		return false;
	for (int n = 0; n < strlen(cmp); n++)
		if (str1[n + off] != cmp[n])
			return false;
	return true;
}

inline int estrcmp(const char *str, const char bit)
{
	int ret = 0;
	for (int n = 0; n < strlen(str); n++)
		if (str[n] == bit)
			ret++;
	return ret;

}

bool estrcmp(const char *str, const char *cmp)
{
	for (int n = 0; n < strlen(str); n++)
	{
		bool find_stat = true;
		for (int m = 0; m < strlen(cmp); m++)
			if (str[n+m] != cmp[m])
				find_stat = false;
		if (find_stat)
			return true;
	}
	return false;
}

inline int estrfind(const char *str, const char bit, const int x)
{
	int find = 0;
	for (int n = 0; n < strlen(str); n++)
	{
		if (str[n] == bit)
			find++;
		if (find == x)
			return n;
	}
	system("cls");
	cout << "Warring of :" << endl << str <<"Bit:"<<bit<<" index="<<x<<" Not Find!"<< endl;
	return -1;
}

inline string strmove(const char *buff, char *remove_list,char replace='\0')
{
	char ret[MAX_BUFF_SIZE] = { 0x00 };
	int g=0;
	for (int n = 0; n < strlen(buff); n++)
	{
		bool find_stat = false;
		for (int b = 0; b < strlen(remove_list); b++)
			if (buff[n] == remove_list[b])
				find_stat = true;
		if (!find_stat)
			ret[g++] = buff[n];
		else
			if (replace != '\0')
			ret[g++] = replace;
	}
	string _ret=ret;
	return _ret;
}

char rl[] = "\"\n";

//#define DEBUG
inline string GetCommand(BLOCK blk)
{
	char buff[MAX_BUFF_SIZE] = {0x00};
	size_t len;
	getenv_s(&len, buff, "cd");
	string localdir = buff;
	switch (mode)
	{
	case clean:
		return "if exist " + strmove(strmove(blk.b.data(), rl).data(), "//", '\\') + " rmdir / q / s " + strmove(strmove(blk.b.data(), rl).data(), "//", '\\');
		break;
	case update:
		return "cd " + strmove(strmove(blk.b.data(), rl).data(), "//", '\\') + "&&git pull " + strmove(blk.b.data(), rl) + " --mirror&&cd " + localdir;
		break;
	case download:
		return "if not exist " + strmove(strmove(blk.b.data(), rl).data(), "//", '\\') + " (mkdir " + strmove(strmove(blk.b.data(), rl).data(), "//", '\\') + ")&&git clone " + MIRROR + strmove(strmove(blk.a.data(), "/", '_').data(), rl) + " " + strmove(strmove(blk.b.data(), rl).data(), "//", '\\') + " --mirror";
		break;
	default:
		cout << "Warring:Unknow Mode!" << endl;
		break;
	}
	return "echo Error Correcped!&&pause>nul";
}
#include <Windows.h>

bool thread_busy = false;



inline void log(string message)
{
	while (thread_busy)
		Sleep(100);
	thread_busy = true;
	cout << "[" << omp_get_thread_num() << "] Processing:" << message << endl;
	thread_busy = false;
	
}

bool debug = false;

#define DEBUG_LINE if (debug) 

int work_project = 0;

void main(int argc, char *argv[])
{
	cout << "Processing....." << endl;
	if (argc < 1)
	{
		cout << "Unknow Filename!" << endl;
		exit(-1);
	}
	input.open(argv[1],ios::_Nocreate);
	for (int n = 0; n < argc;n++)
		if (argv[n][0] == '-' && argv[n][1] == '-')
		{
		if (!strcmp(argv[n], "--clean"))
			mode = clean;
		if (!strcmp(argv[n], "--download"))
			mode = download;
		if (!strcmp(argv[n], "--update"))
			mode = update;
		if (!strcmp(argv[n], "--verbos"))
			debug = true;
		}
	if (!input.is_open())
	{
		cout << "File IO Except!" << endl;
		exit(-1);
	}
	int line = 0;
	while (!input.eof())
	{
		string sbuff;
		char buff[MAX_BUFF_SIZE];
		memset(buff, 0, sizeof(buff));
		input.getline(buff, MAX_BUFF_SIZE);
		sbuff = buff;
		bool stat = false;
		if (!estrcmp(buff, "project") || estrcmp(buff, "</"))
			continue;
		//cout << "Processing:" << sbuff << endl;
		string a = sbuff.substr(estrfind(buff, '\"', 1), estrfind(buff, '\"', 2) - estrfind(buff, '\"', 1));
		string b = sbuff.substr(estrfind(buff, '\"', 3), estrfind(buff, '\"', 4) - estrfind(buff, '\"', 3));
		DEBUG_LINE cout << "Worddir:" << a << endl << "Download_Dir:" << b << endl;
		BLOCK temp;
		temp.a = b;
		temp.b = a;
		poll.push_back(temp);
		work_project++;
	}
	cout << "Exec......" << endl;
	cout << "共:" << work_project << "个项目" << endl;
	DEBUG_LINE omp_set_num_threads(1);
	else
		omp_set_num_threads(32);
	int worked = 0;
#pragma omp parallel for
	for (int n = 0; n < poll.size(); n++)
	{
		string cmd = GetCommand(poll.at(n));
		DEBUG_LINE log(cmd);
		system(cmd.data());
		worked++;
		char buff[MAX_BUFF_SIZE] = { '\0' };
		_itoa_s(work_project - worked, buff, 06);
		
		string lo = "剩余:";
			lo+=buff;
			log(lo);
	}

}